# frozen_string_literal: true

puts 'my string'

puts 'string with "quotes"'

puts "string with \nnew line"

my_variable = '    phrase    '
puts my_variable

puts my_variable.upcase

puts my_variable.strip

puts my_variable.length

puts my_variable.include? 'ras'

puts my_variable.include? 'banana'

my_variable = 'phrase'

puts my_variable[0]

puts my_variable[0, 3] # non inclusive of the 3rd

puts my_variable.index('h')

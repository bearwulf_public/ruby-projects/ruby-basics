# frozen_string_literal: true

# You can only have unique keys in hashes
states = {
  'Wisconsin' => 'WI',
  'Pennsylvania' => 'PA',
  'Oregon' => 'OR'
}

puts states

puts states['Wisconsin']
puts states['Oregon']
puts states['waldo'] # This just returns null ruby does not throw a key error here

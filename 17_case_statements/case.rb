# frozen_string_literal: true

def day_abbreviations_to_day(abbreviation)
  case abbreviation
  when 'mon'
    'Monday'
  when 'tue'
    'Tuesday'
  when 'wed'
    'Wednesday'
  when 'thu'
    'Thursday'
  when 'fri'
    'Friday'
  when 'sat'
    'Saturday'
  when 'sun'
    'Sunday'
  else
    'Invalid Abbreviation'
  end
end

puts(day_abbreviations_to_day('mon'))
puts(day_abbreviations_to_day('potato'))

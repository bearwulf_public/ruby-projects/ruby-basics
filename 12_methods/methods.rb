# frozen_string_literal: true

# Methods

def say_hi_to_the_user(user, age = -1)
  puts("Hello #{user}you are ", age.to_s.chomp)
end

say_hi_to_the_user('Zak')

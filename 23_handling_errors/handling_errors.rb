# frozen_string_literal: true

lucky_nums = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

# begin, rescue, end example
begin
  # num = 10/0
  lucky_nums['dog']
rescue ZeroDivisionError => e
  puts('Zero division error')
  puts(e)
rescue TypeError => e
  puts('incorrect type')
  puts(e)
end

# frozen_string_literal: true

friends = Array['Meghan', 'Kennedy', 'Chris', 'Camden', true, 5] # Arrays do not need to be one data type

puts friends[0]
puts friends[-2]
puts friends.length
puts friends.include? 'Meghan'
puts friends.reverse

# puts friends.sort # This cannot be used when you have multiple data types

puts(friends[0, 3])

friends[5] = 'randy' # Can rewrite when needed

classes = []

classes[0] = '575'

puts classes

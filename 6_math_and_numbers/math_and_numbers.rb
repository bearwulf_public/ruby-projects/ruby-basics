# frozen_string_literal: true

puts 5

puts 5.87594

puts(-5)

puts 5 + 9

puts 5 - 9

puts 5 / 9

puts 5 % 9

puts 5**3

puts 5 * 5

num = -20

puts num

puts("my fav num #{num}")

puts num.abs

num2 = 20.2

puts num2.round

puts num2.ceil
puts num2.floor

puts Math.sqrt(num2)

puts 3 + 4

puts 3.0 + 5

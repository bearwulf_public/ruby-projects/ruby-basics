# frozen_string_literal: true

File.open('22_writing_files/myFile.txt', 'w+') do |file|
  file.write('Here is some data. Wow so cool')
end

# frozen_string_literal: true

puts('Enter First Number: ')
num1 = gets.chomp.to_f
puts('Enter the operator: ')
operator = gets.chomp
puts('Enter the second number: ')
num2 = gets.chomp.to_f

# This is major garbage but it describes how if statements chain and how to get user input.
case operator
when '+'
  puts num1 + num2
when '-'
  puts num1 - num2
when '/'
  puts num1 / num2
when '*'
  puts num1 * num2
when '**'
  puts num1**num2
when '%'
  puts num1 % num2
else
  puts('Invalid operator')
end

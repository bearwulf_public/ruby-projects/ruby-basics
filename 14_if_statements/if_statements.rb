# frozen_string_literal: true

ismale = true
is_tall = false

# Ruby uses and/or syntax for the logic
if ismale && is_tall
  puts('you are a male and tall')
else
  puts('you are either not male or not tall or both')
end

# Else/if logic
if is_tall
  puts('you are tall')
elsif !ismale
  puts('you are male')
else
  puts('you are something else')
end

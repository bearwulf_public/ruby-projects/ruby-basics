# frozen_string_literal: true

class DifferentBook
  attr_accessor :title, :author, :pages

  def initialize(title, author, pages)
    @title = title
    @author = author
    @pages = pages
  end
end

book1 = DifferentBook.new('Harry Potter', 'JK Rowling', 500)

puts(book1.pages)

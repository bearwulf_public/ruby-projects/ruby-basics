# frozen_string_literal: true

class Human
  def breaths
    puts "the human breaths"
  end
end

class Chef
  def make_chicken
    puts "The chef makes chicken"
  end

  def make_salad
    puts "the chef makes Salad"
  end

  def make_special
    puts "the chef makes special dish"
  end
end

class ItalianChef < Chef # Ruby does not allow for python like multi inheritance for that we need modules
  # @overload make_special()
  def make_special # you can do overrides
    puts "the chef makes pizza"
  end
end

italian_chef = ItalianChef.new
italian_chef.make_salad
italian_chef.make_special

# frozen_string_literal: true

# this is an ugly but beginner language friendly way to do this
# Notice the path here. Since the IDE is running this the pathing starts from the root of the project
File.open('21_reading_files/employees.txt', 'r') do |file| # Do is acting as a context manager here
  # puts(file.read()) # File exists in memory but once read is done we are at EOL. the readline method does not work with read together.
  # files are static in memory not instanced off of with calls to file

  # puts(file.readline())

  file.readlines.each do |line|
    puts(line)
    puts(line[0])
  end
end

## BUT you can do this without a context manager if that is not needed

file = File.open('21_reading_files/employees.txt')
puts(file.read)
file.close

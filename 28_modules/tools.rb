# frozen_string_literal: true

module Tools
  def sayhi(name)
    puts("hello #{name}")
  end

  def saybyte(name)
    puts("bye #{name}")
  end
end

# frozen_string_literal: true

print 'Zak Seipel'
puts 'Waldo'

puts ''

puts 'Waldo' # adds new lines to end
print 'Zak Seipel' # Does not add new lines to end

puts ''

puts '   /|'
puts '  / |'
puts ' /  |'
puts '/___|'

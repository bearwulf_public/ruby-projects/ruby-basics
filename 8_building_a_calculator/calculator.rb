# frozen_string_literal: true

# puts "Enter a Number: "
# num1 = gets.chomp
#
# puts "Enter another Number: "
# num2 = gets.chomp
#
#
# puts(num1 + num2) # this just causes it to concatinate two strings together.

puts 'Enter a Number: '
num1 = gets.chomp
# num1 = gets.chomp.to_f Can convert at instance ot retrieval not at variable level if you want to
puts 'Enter another Number: '
num2 = gets.chomp

puts(num1.to_i + num2.to_i) # Cast to an integer

# but if you enter a floating point number
# EX: 5 + 2.5
# The result will be 7 not 7.5

# This is due to the casting that is occuring

puts(num1.to_f + num2.to_f)

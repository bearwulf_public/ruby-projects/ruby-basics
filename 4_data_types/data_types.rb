# frozen_string_literal: true

# going through different basic data types

# Strings
name = 'Zak'
puts name
occupation = 'SWE'
puts occupation

# integers
age = 29
puts age

# Floating point
gpa = 4.0
puts gpa

# Negative Numbers
magic_var = -1.2
puts magic_var

# Boolean
is_employed = true
puts is_employed

# Nil values
nil # Cannot print nil variables

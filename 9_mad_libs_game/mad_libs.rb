# frozen_string_literal: true

puts('Enter a color: ')
color = gets.chomp

puts('Enter a Plural Noun: ')
plural_noun = gets.chomp

puts('Enter a celebrity: ')
celebrity = gets.chomp

puts("Roses are #{color}")
puts("#{plural_noun} are Blue")
puts("I love #{celebrity}")

puts(celebrity[0])

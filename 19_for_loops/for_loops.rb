# frozen_string_literal: true

friends = %w[Meghan Aaron Chris]

friends.each do |friend|
  puts(friend)

  # Special Ruby syntax of a for loop
  puts i
end

# Looping N numer of times
(0..5).each do |index|
  puts(index)
end

# Special ruby syntax
6.times do |j|
  puts j
end

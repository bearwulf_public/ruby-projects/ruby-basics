# frozen_string_literal: true

def cube_number(input_number)
  input_number**3 # last line is always returned no matter what
end

def cube_number_returns_five(input_number)
  input_number**3
  5 # returns this no matter what
end

def cube_number_with_return(input_number)
  num = input_number**3
  [num, input_number] # Can return multiple values and this turns into an array like Python
  # return nil no real way to have a void method then?
end

puts(cube_number(3))
puts(cube_number_returns_5(3))
puts(cube_number_with_return(3))

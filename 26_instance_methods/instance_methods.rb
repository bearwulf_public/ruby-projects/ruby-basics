# frozen_string_literal: true

class Student
  attr_accessor :name, :major, :gpa

  def initialize(name, major, gpa)
    @name = name
    @major = major
    @gpa = gpa
  end

  def has_honors
    return true if @gpa >= 3.5

    false
  end
end

student1 = Student.new('jim', 'Business', 2.6)
puts(student1.has_honors)

student2 = Student.new('pam', 'art', 3.6)
puts(student2.has_honors)

# frozen_string_literal: true

character_name = 'Waldo' # Ruby does not have static typing
character_age = '58' # errors thrown if concat numbers and strings because of the notation above?

puts "There was once a man named #{character_name}"
puts "he was #{character_age} years old."

character_name = 'Zak' # Can modify variables as needed

puts "He really liked the name #{character_name}"
puts "He did not like being #{character_age}."
